'use strict';

const config = require('./config');

function paraqueue(limit) {
	limit = limit || config.parallel;
	const queue = [];
	let running = 0;

	async function start(job) {
		try {
			try {
				job.res(await job.act());
			} catch (e) {
				job.rej(e);
			}
		} finally {
			running--;
			checkstart();
		}
	}

	function checkstart() {
		while(queue.length && (running < limit)) {
			const job = queue.shift();
			running++;
			start(job);
		}
	}

	function wrap(act) {
		let res, rej;
		const prom = new Promise((r, j) => {
			res = r;
			rej = j;
		});
		return {
			act,
			res,
			rej,
			prom
		};
	}

	function push(act) {
		act = wrap(act);
		queue.push(act);
		checkstart();
		return act.prom;
	}

	function unshift(act) {
		act = wrap(act);
		queue.unshift(act);
		checkstart();
		return act.prom;
	}
	push.push = push;
	push.unshift = unshift;
	return push;
}

module.exports = paraqueue;
