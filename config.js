'use strict';

const os = require('os');
const process = require('process');
const minimist = require('minimist');

const config = minimist(process.argv.slice(2), {
	default: {
		parallel: os.cpus()
			.length,
		layers: true,
		trim: true,
		pad: true,
		hide: true,
		labelsplit: '|'
	}
});

console.log(`config:${JSON.stringify(config)}`);

if(config.outdir)
	for(const k of ['pngout', 'luaout', 'jsonout'])
		if(config[k] === true)
			config[k] = config.outdir;

if(!config.pngout)
	throw Error('--pngout= must be specified.');
if(!config.luaout && !config.jsonout)
	throw Error('At least one of --luaout= or --jsonout= must be specified.');

module.exports = config;
