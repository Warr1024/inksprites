'use strict';

const path = require('path');
const fs = require('fs');
const config = require('./config');

const valrx = /^[A-Za-z_][A-Za-z0-9_]*$/;

function luafy(x) {
	if(Array.isArray(x))
		return `{${x.map(luafy).join(',')}}`;
	if(x && x.toString() === '[object Object]')
		return `{${Object.keys(x).map(k => (valrx.test(k) ? k : `[${luafy(k)}]`)
		+ `=${luafy(x[k])}`).join(',')}}`;
	return JSON.stringify(x);
}

async function writeout(pathspec, deffile, data) {
	const stat = await new Promise((r, j) => fs.stat(config.luaout,
		(e, s) => (e && e.code !== 'ENOENT') ? j(e) : r(s)));
	if(stat && stat.isDirectory())
		pathspec = path.join(pathspec, deffile);
	await new Promise((r, j) => fs.writeFile(pathspec, data, e => e ? j(e) : r()));
}

async function outproc(svgpath, data) {
	const basename = path.basename(svgpath)
		.replace(/(\.[A-Za-z0-9]{1,4})+$/, '');
	const tasks = [];
	data.sort((a, b) => a.zorder - b.zorder);
	const final = {};
	data.map(e => {
		const data = {
			img: [
					basename,
					e.frame,
					e.zorder && ('000' + e.zorder)
					.substr(-3),
					e.layer
				]
				.filter(x => x)
				.map(x => config.labelsplit ?
					x.toString()
					.split(config.labelsplit)[0] :
					x.toString())
				.filter(x => x)
				.join(' ')
				.toLowerCase()
				.replace(/[^a-z0-9]+/g, ' ')
				.trim()
				.replace(/ /g, '_'),
			layer: e.layer,
			hide: config.hide && e.hidden,
			x: e.x,
			y: e.y,
			w: e.w,
			h: e.h,
		};
		tasks.push(new Promise((r, j) => fs.copyFile(e.png,
			path.join(config.pngout, `${config.pngprefix || ''}${data.img}.png`),
			e => e ? j(e) : r())));
		Object.keys(data)
			.filter(k => !data[k])
			.forEach(k => delete data[k]);
		const f = e.frame || 'main';
		(final[f] = final[f] || [])
		.push(data);
	});
	if(config.luaout)
		tasks.push(writeout(config.luaout, `${basename}.lua`, `return ${luafy(final)}`));
	if(config.jsonout)
		tasks.push(writeout(config.jsonout, `${basename}.json`, JSON.stringify(final)));
	await Promise.all(tasks);
}

module.exports = outproc;
