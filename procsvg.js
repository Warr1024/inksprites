'use strict';

const fs = require('fs');
const path = require('path');
const tmp = require('tmp');
const xmljs = require('xml-js');
const analyze = require('./analyze');
const config = require('./config');
const inkscape = require('./inkscape');
const pngtrim = require('./pngtrim');
const paraqueue = require('./paraqueue');
const outproc = require('./outproc');
const walkelements = require('./walkelements');

async function procframe(data, svgpath, frameid, opt, w, h, entry) {
	let s;
	let sopt = {};
	if(config.width && (!s || ((s * w) > config.width))) {
		s = config.width / w;
		sopt = { w: config.width };
	}
	if(config.height && (!s || ((s * h) > config.height))) {
		s = config.height / w;
		sopt = { h: config.height };
	}
	if(config.pixels && (!s || ((s * w * s * h) > config.pixels))) {
		s = Math.sqrt(config.pixels / w / h);
		sopt = { d: 96 * s };
	}
	Object.assign(opt, sopt);
	opt[''] = svgpath;
	opt.o = svgpath.replace(/\.svg$/, `_${frameid}.png`);
	opt['export-type'] = 'png';
	opt.y = '0';
	const label = `${path.basename(data.svgpath)} ${
		entry.frame ? `frame ${JSON.stringify(entry.frame)}` : 'page'}${
		entry.layer ? ` layer ${JSON.stringify(entry.layer)}` : ''}`;
	opt.log = `render: ${label}`;
	await inkscape(opt);
	entry.png = opt.o;
	Object.assign(entry, await pngtrim(opt.o, label));
	if(!entry.blank)
		data.rendered.push(entry);
}

async function procframes(data, tmpdir, layerid, xmltext, entry) {
	const svgpath = path.join(tmpdir, `${layerid}.svg`);
	await new Promise((r, j) => fs.writeFile(svgpath,
		xmltext, e => e ? j(e) : r()));
	if(data.frames) {
		let frameid = 0;
		await Promise.all(data.frames.map(f =>
			procframe(data, svgpath, ++frameid, { i: f.id }, f.w, f.h,
				Object.assign({ zorder: layerid, frame: f.frame }, entry))));
	} else
		await procframe(data, svgpath, 0, { C: true },
			data.page.w, data.page.h, Object.assign({ zorder: layerid }, entry));
}

const myqueue = paraqueue();
async function procsvg(svgpath) {
	const data = await analyze(svgpath);
	data.rendered = [];

	walkelements(data.xml, e => {
		if(e.name === 'svg') {
			e.attributes['inkscape:pageopacity'] = '0';
			return true;
		}
	});
	if(data.frames)
		data.frames.forEach(f => walkelements(data.xml, e => {
			if(e.attributes && e.attributes.id === f.id)
				e.attributes.style += ';opacity: 0';
		}));

	const tmpdir = await new Promise((r, j) =>
		tmp.dir({ mode: 448, unsafeCleanup: true }, (e, p, c) =>
			e ? j(e) : r({ path: p, cleanup: c })));
	try {
		if(data.layers) {
			const tasks = [];
			let layerid = 0;
			data.layers.forEach(layer => {
				const id = ++layerid;
				data.layers.forEach(l =>
					l.node.attributes.style = (l.node === layer.node) ?
					l.style : 'display: none');
				const txt = xmljs.js2xml(data.xml);
				tasks.push(myqueue.push(() =>
					procframes(data, tmpdir.path, id, txt, {
						layer: layer.label,
						hidden: layer.hidden
					})));
			});
			await Promise.all(tasks);
		} else {
			const txt = xmljs.js2xml(data.xml);
			await procframes(data, tmpdir.path, 0, txt, {});
		}
		await outproc(data.svgpath, data.rendered);
	} finally {
		tmpdir.cleanup();
	}
}

module.exports = procsvg;
