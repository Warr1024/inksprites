'use strict';

const config = require('./config');
const procsvg = require('./procsvg');
const paraqueue = require('./paraqueue');

if(!config._ || !config._.length)
	throw Error('No input SVG files specified.');

const myqueue = paraqueue();
config._.forEach(s => myqueue.push(() => procsvg(s)));
