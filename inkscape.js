'use strict';

const path = require('path');
const childproc = require('child_process');
const paraqueue = require('./paraqueue');

const myqueue = paraqueue();

async function inkscape(opts, ...moreargs) {
	const log = opts.log;
	delete opts.log;
	if(opts.hasOwnProperty('z'))
		opts.z = true;
	const args = [];
	Object.keys(opts)
		.filter(k => opts[k])
		.forEach(k => {
			if(k.length === 1) {
				args.push(`-${k}`);
				if(opts[k] !== true)
					args.push(opts[k].toString());
			} else if(k.length > 1) {
				let s = `--${k.replace(/_/g, '-')}`;
				if(opts[k] !== true)
					s += `=${opts[k].toString()}`;
				args.push(s);
			}
		});
	if(opts['']) args.push(opts['']);
	if(moreargs)
		Array.prototype.push.apply(args, moreargs);
	return await myqueue.push(async () => {
		if(log) console.log(log);
		console.log(['> inkscape'].concat(args)
			.map(x => path.basename(x))
			.join(' '));
		const cp = childproc.spawn('inkscape', args, {
			stdio: ['ignore', 'pipe', 'inherit']
		});
		let stdout = '';
		cp.stdout.on('data', x => stdout += x.toString());
		return await new Promise((r, j) => {
			cp.on('error', j);
			cp.on('exit', (c, s) => {
				if(s) return j(`signal ${s}`);
				if(c) return j(`code ${c}`);
				return r(stdout);
			});
		});
	});
}

module.exports = inkscape;
