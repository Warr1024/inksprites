'use strict';

function walkelements(root, func) {
	const r = func(root);
	if(r) return r;
	if(root.elements)
		for(const e of root.elements) {
			const s = walkelements(e, func);
			if(s) return s;
		}
}

module.exports = walkelements;