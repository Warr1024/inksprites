'use strict';

function pngpad(data, width, minx, miny, maxx, maxy) {
	let r = 0;
	let g = 0;
	let b = 0;
	let a = 0;
	const mix = (x, y) => {
		const idx = (y * width + x) * 4;
		if(idx >= 0 && idx < data.length) {
			const ax = data[idx + 3];
			if(ax) {
				r += data[idx] * ax;
				g += data[idx + 1] * ax;
				b += data[idx + 2] * ax;
				a += ax;
			}
		}
	};
	const commit = {};
	for(let y = miny; y <= maxy; y++)
		for(let x = minx; x <= maxx; x++) {
			const idx = (y * width + x) * 4;
			if(data[idx + 3] <= 0) {
				r = 0; g = 0; b = 0; a = 0;
				for(let dy = -1; dy <= 1; dy++)
					for(let dx = -1; dx <= 1; dx++)
						mix(x + dx, y + dy);
				if(a) {
					commit[idx] = Math.floor(r / a);
					commit[idx + 1] = Math.floor(g / a);
					commit[idx + 2] = Math.floor(b / a);
					commit[idx + 3] = 1;
				}
			}
		}
	Object.keys(commit)
		.forEach(k => data[k] = commit[k]);
}

module.exports = pngpad;
