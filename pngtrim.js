'use strict';

const fs = require('fs');
const pngjs = require('pngjs');
const config = require('./config');
const pngpad = require('./pngpad');

async function pngtrim(pngpath, label) {
	console.log(`trim: ${label || pngpath}`);
	const png = new pngjs.PNG();
	fs.createReadStream(pngpath)
		.pipe(png);
	await new Promise((r, j) => {
		png.on('metadata', md => {
			if(md.palette) j('Palette images not supported.');
			if(!md.color) j('Non-color images not supported.');
			if(!md.alpha) j('Non-alpha images not supported.');
		});
		png.on('error', j);
		png.on('parsed', r);
	});

	let maxx = 0;
	let minx = png.width;
	let maxy = 0;
	let miny = png.height;
	let idx = 0;
	for(let y = 0; y < png.height; y++)
		for(let x = 0; x < png.width; x++) {
			if(png.data[idx + 3]) {
				if(x > maxx) maxx = x;
				if(x < minx) minx = x;
				if(y > maxy) maxy = y;
				if(y < miny) miny = y;
			}
			idx += 4;
		}
	if((minx > maxx) || (miny > maxy))
		return { blank: true };

	if(!config.trim)
		return {};

	if(config.pad) {
		if(minx > 0) minx--;
		if(maxx < (png.width - 1)) maxx++;
		if(miny > 0) miny--;
		if(maxy < (png.height - 1)) maxy++;
		pngpad(png.data, png.width, minx, miny, maxx, maxy);
	}

	const newpng = new pngjs.PNG({
		width: maxx - minx + 1,
		height: maxy - miny + 1
	});
	png.bitblt(newpng, minx, miny, newpng.width, newpng.height, 0, 0);

	const outpath = pngpath.replace(/\.png$/, '_opt.png');
	const outstr = fs.createWriteStream(outpath);
	newpng.pack().pipe(outstr);
	await new Promise((r, j) => {
		outstr.on('error', j);
		outstr.on('close', r);
	});

	return {
		x: minx,
		y: miny,
		w: newpng.width,
		h: newpng.height,
		png: outpath
	};
}

module.exports = pngtrim;
