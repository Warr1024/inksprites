'use strict';

const fs = require('fs');
const xmljs = require('xml-js');
const inkscape = require('./inkscape');
const config = require('./config');
const walkelements = require('./walkelements');

function conv(txt) {
	const num = Number(txt.replace(/\D+$/, ''));
	if(txt.endsWith('mm'))
		return num * 3.779528;
	return num;
}

async function analyze(svgpath) {
	console.log(`analyze: ${svgpath}`);

	const data = { svgpath };

	data.xml = new Promise((r, j) => fs.readFile(svgpath,
			(e, d) => e ? j(e) : r(d.toString())))
		.then(async s => await xmljs.xml2js(s));

	if(config.layers)
		data.layers = data.xml.then(svgxml => {
			const displaynone = /\bdisplay\s*:\s*none\b/g;
			const layers = [];
			walkelements(svgxml, e => {
				if(e.type === 'element' &&
					e.attributes &&
					e.attributes['inkscape:groupmode'] === 'layer')
					layers.push(e);
			});
			if(!layers.length)
				throw Error(`No layers found in ${JSON.stringify(svgpath)}`);
			return layers.map(l => ({
				label: l.attributes['inkscape:label'] || l.attributes.id,
				hidden: displaynone.test(l.attributes.style),
				style: (l.attributes.style || '')
					.replace(displaynone, ''),
				node: l
			}));
		});

	if(config.frames)
		data.frames = inkscape({ '': svgpath, S: true })
		.then(raw => {
			const list = raw.match(/[^\r\n]+/g)
				.map(l => {
					const a = l.split(',');
					return { id: a[0], w: a[3], h: a[4] };
				});
			const rx = new RegExp(config.frames);
			const frames = [];
			list.forEach(x => {
				const m = x.id.match(rx);
				if(!m) return;
				x.frame = (m.length > 1) ? m.slice(1)
					.join('') : x.id;
				frames.push(x);
			});
			if(!frames.length)
				throw Error(`No frames found in ${JSON.stringify(svgpath)}`);
			return frames;
		});
	else
		data.page = data.xml
		.then(x => walkelements(x, e => {
			if(e.name === 'svg')
				return {
					w: conv(e.attributes.width),
					h: conv(e.attributes.height)
				};
		}));

	await Promise.all(Object.keys(data)
		.map(async k => data[k] = await data[k]));

	return data;
}

module.exports = analyze;
